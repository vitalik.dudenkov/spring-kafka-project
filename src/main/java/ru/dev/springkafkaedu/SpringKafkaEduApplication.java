package ru.dev.springkafkaedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringKafkaEduApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringKafkaEduApplication.class, args);
	}

}
